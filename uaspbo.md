# 1. Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital

| No. | Use Case                                                | Prioritas |
|-----|--------------------------------------------------------|-----------|
| 1   | Pencarian Penerbangan                                  | 9         |
| 2   | Membandingkan Harga Penerbangan                        | 8         |
| 3   | Menyimpan Penerbangan Favorit                          | 6         |
| 4   | Menyaring Penerbangan Berdasarkan Maskapai              | 6         |
| 5   | Menyaring Penerbangan Berdasarkan Jadwal                | 7         |
| 6   | Menyaring Penerbangan Berdasarkan Harga                 | 7         |
| 7   | Melihat Informasi Detail Penerbangan                   | 9         |
| 8   | Memilih Kursi Pada Penerbangan                          | 8         |
| 9   | Memasukkan Data Penumpang                               | 9         |
| 10  | Memilih Metode Pembayaran                               | 9         |
| 11  | Melakukan Pembayaran dengan Kartu Kredit                | 9         |
| 12  | Melakukan Pembayaran dengan Transfer Bank               | 8         |
| 13  | Menerima Konfirmasi Pembayaran                          | 9         |
| 14  | Mengunduh Tiket Elektronik                              | 8         |
| 15  | Membatalkan Penerbangan                                 | 6         |

# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital


```mermaid
classDiagram
    class Penerbangan {
        -kodePenerbangan: String
        -maskapai: String
        -jadwal: DateTime
        -harga: float
        -rute: String
        +getKodePenerbangan(): String
        +getMaskapai(): String
        +getJadwal(): DateTime
        +getHarga(): float
        +getRute(): String
    }

    class PencarianPenerbangan {
        +cariPenerbangan(rute: String, jadwal: DateTime): List<Penerbangan>
    }

    class MembandingkanHarga {
        +bandingkanHarga(penerbangan: List<Penerbangan>): Penerbangan
    }

    class InformasiPenerbangan {
        +getInformasiPenerbangan(kodePenerbangan: String): Penerbangan
    }

    class Pembayaran {
        +pilihMetodePembayaran(): void
        +lakukanPembayaran(metodePembayaran: String, totalPembayaran: float): boolean
        +terimaKonfirmasiPembayaran(): void
    }

    class TiketElektronik {
        -kodeTiket: String
        -penumpang: String
        -penerbangan: Penerbangan
        +getKodeTiket(): String
        +getPenumpang(): String
        +getPenerbangan(): Penerbangan
        +unduhTiketElektronik(): void
    }

    class PembatalanPenerbangan {
        +batalkanPenerbangan(kodeTiket: String): void
        +ubahJadwalPenerbangan(kodeTiket: String, jadwalBaru: DateTime): void
    }

    class PengelolaAkun {
        +kelolaInformasiAkun(): void
        +mengelolaPengaturanLainnya(): void
    }

    class PemesananTiket {
        +pesanTiket(penerbangan: Penerbangan, penumpang: String): TiketElektronik
    }

    class KursiPenerbangan {
        -nomorKursi: String
        +getNomorKursi(): String
    }

    class Penumpang {
        -nama: String
        -umur: int
        -jenisKelamin: String
        +getNama(): String
        +getUmur(): int
        +getJenisKelamin(): String
    }

    PencarianPenerbangan --> Penerbangan
    MembandingkanHarga --> Penerbangan
    InformasiPenerbangan --> Penerbangan
    PemesananTiket --> Penerbangan
    PemesananTiket --> Penumpang
    PemesananTiket --> TiketElektronik
    Pembayaran --> TiketElektronik
    TiketElektronik --> Penerbangan
    PembatalanPenerbangan --> TiketElektronik
    PengelolaAkun --> Penumpang
    PemesananTiket --> KursiPenerbangan

```

# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

Berikut ini adalah penunjukan bagian kode yang mencerminkan penerapan prinsip SOLID:

1. Single Responsibility Principle (SRP):
```
`$conn = mysqli_connect($host, $username, $password, $dbname) or die(mysqli_error());
// Memeriksa koneksi
if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}
`
```
2. Mengambil data hotel Open/Closed Principle (OCP):
```
`// Memeriksa apakah parameter id diberikan
if (isset($_GET["id"])) {
    $hotelId = $_GET["id"];
    // Menghapus hotel berdasarkan ID
    $sqlDelete = "DELETE FROM hotels WHERE id = $hotelId";
    if ($conn->query($sqlDelete) === TRUE) {
        echo "Hotel berhasil dihapus.";
    } else {
        echo "Error: " . $sqlDelete . "<br>" . $conn->error;
    }
} else {
    echo "Parameter id tidak ditemukan.";
}
`
```

# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

- Model-View-Controller (MVC):

Kode ini mencoba memisahkan logika bisnis, tampilan, dan interaksi dengan pengguna. Bagian yang mengambil data hotel dari database dan menangani aksi penghapusan hotel dapat dikategorikan sebagai bagian model, sedangkan tampilan HTML dan formulir pemesanan hotel termasuk dalam bagian view. Pola ini memberikan pemisahan tanggung jawab yang jelas antara komponen-komponen aplikasi.

- Front Controller:

Kode ini menggunakan satu file PHP sebagai titik masuk tunggal (index.php) yang menangani semua permintaan dan routing ke fungsi-fungsi yang relevan. Ini memungkinkan pemisahan logika bisnis dari tampilan dan mengatur aliran kontrol secara terpusat.

- Post-Redirect-Get (PRG):

Kode ini mengikuti pola PRG untuk menghindari resubmission data ketika pengguna me-refresh halaman setelah melakukan tindakan (misalnya menghapus hotel). Setelah menghapus hotel, halaman akan di-redirect kembali ke halaman daftar hotel menggunakan fungsi header("Location: hapus.php?id=$hotelId"); sehingga pengguna tidak dapat secara tidak sengaja melakukan tindakan tersebut lagi dengan merefresh halaman.

# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database
![dokumentasi](dokumentasi/contoh1.png)

Kode tersebut mencakup berbagai aspek yang terlibat dalam proses pemesanan tiket dan tampilan histori pemesanan tiket, serta menggunakan library Bootstrap untuk mengatur tampilan halaman web.

![dokumentasi](dokumentasi/contoh2.png)

Kodingan di atas berfungsi untuk menambahkan data hotel baru ke dalam database menggunakan PHP. Fungsinya secara singkat dan jelas adalah:

1. Membuat koneksi ke database MySQL.
2. Memeriksa koneksi dan menampilkan pesan error jika terjadi kegagalan.
3. Memeriksa apakah parameter yang diperlukan (id, nama_hotel, lokasi, harga) telah diberikan melalui metode POST.
4. Jika parameter diterima, menyimpan nilai-nilai parameter ke dalam variabel yang sesuai.
5. Melakukan perintah SQL INSERT untuk menambahkan data hotel baru ke dalam tabel "hotels" pada database.

![dokumentasi](dokumentasi/contoh3.png)

Kodingan di atas adalah sebuah halaman web yang menampilkan daftar hotel dan menyediakan fitur untuk menambahkan dan menghapus hotel. Berikut adalah fungsi utama dari kodingan:
1. Membuat koneksi ke database MySQL.
2. Mengambil data hotel dari tabel "hotels" dan menampilkannya dalam bentuk tabel.
3. Memproses permintaan penghapusan hotel dan mengarahkan ke halaman "hapus.php" dengan parameter hotelId.
4. Menampilkan form untuk menambahkan hotel baru dengan metode POST.
5. Menggunakan Bootstrap untuk tampilan yang responsif.


# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya 
![dokumentasi](dokumentasi/create.png)

tampilan diatas berfungsi untuk menambah hotel

![dokumentasi](dokumentasi/read.png)

tampilan diatas berfungsi untuk menalpilkan informasi hotel pada User

![dokumentasi](dokumentasi/delete.png)

tampilan diatas berfungsi untuk menghapus data hotel yang dipilih
# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital

- Tabel Daftar Hotel: Tabel ini menampilkan daftar hotel dengan kolom-kolom berikut:

- Id Hotel: Menampilkan id hotel dari setiap entri.
Nama Hotel: Menampilkan nama hotel dari setiap entri.
Lokasi: Menampilkan lokasi hotel dari setiap entri.
Harga: Menampilkan harga hotel dari setiap entri.
Aksi: Menampilkan tombol "Hapus" untuk menghapus hotel tertentu.

- Tombol "Hapus": Setiap baris dalam tabel daftar hotel memiliki tombol "Hapus" yang memungkinkan pengguna menghapus hotel tertentu dari daftar. Tombol ini dikaitkan dengan fungsi untuk mengarahkan pengguna ke halaman "hapus.php" dengan parameter id hotel yang akan dihapus.

- Formulir Tambah Hotel: Bagian ini berisi formulir untuk menambahkan hotel baru ke daftar. Formulir ini memiliki kolom input berikut:

- Id Hotel: Kolom input untuk memasukkan id hotel baru.
Nama Hotel: Kolom input untuk memasukkan nama hotel baru.
Lokasi: Kolom input untuk memasukkan lokasi hotel baru.
Harga: Kolom input untuk memasukkan harga hotel baru.
Tombol "Tambah Hotel": Tombol ini digunakan untuk mengirimkan formulir tambah hotel. Setelah tombol ini ditekan, data yang dimasukkan akan diproses oleh skrip "tambah_hotel.php" yang ditentukan dalam atribut action formulir.

# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

1. Ketika pengguna mengakses halaman dengan URL yang sesuai, server menghubungkan ke database MySQL menggunakan parameter koneksi seperti host, username, password, dan nama database.

2. Setelah koneksi berhasil didirikan, server mengirimkan permintaan SQL ke database untuk mengambil data hotel menggunakan perintah SELECT.

3. Database mengolah permintaan dan mengembalikan hasil query ke server.

4. Server menerima data hotel dari database dan memprosesnya untuk menghasilkan tampilan tabel dalam HTML.

5. Server mengirimkan tampilan HTML beserta CSS dan JavaScript terkait ke peramban pengguna.

6. Peramban pengguna menerima tampilan HTML, CSS, dan JavaScript, dan mengurai halaman web sesuai dengan instruksi yang terdapat dalam kode.

7. Ketika pengguna melakukan tindakan seperti menghapus hotel, peramban mengirimkan permintaan HTTP POST ke server dengan data yang terkait, seperti ID hotel yang akan dihapus.

8. Server menerima permintaan HTTP POST dan memprosesnya. Jika ada data yang perlu dihapus, server mengarahkan pengguna ke halaman "hapus.php" dengan parameter ID hotel yang akan dihapus melalui header Location.

9. Peramban pengguna mengikuti pengalihan (redirect) ke halaman "hapus.php" dengan parameter yang diberikan.

10. Proses serupa terjadi pada halaman "hapus.php", di mana server berinteraksi dengan database untuk menghapus entri hotel yang sesuai.

11. Setelah operasi selesai, server memberikan respons ke peramban, yang dapat mencakup pesan sukses atau kesalahan.

12. Peramban menerima respons dari server dan menampilkan pesan sukses atau kesalahan kepada pengguna.

# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

# 10. Bonus: Mendemonstrasikan penggunaan Machine Learning
