# 1.Menampilkan use case table untuk produk digitalnya

| No. | Use Case                                                | Prioritas |
|-----|--------------------------------------------------------|-----------|
| 1   | Pencarian Penerbangan                                  | 9         |
| 2   | Membandingkan Harga Penerbangan                        | 8         |
| 3   | Menyimpan Penerbangan Favorit                          | 6         |
| 4   | Menyaring Penerbangan Berdasarkan Maskapai              | 6         |
| 5   | Menyaring Penerbangan Berdasarkan Jadwal                | 7         |
| 6   | Menyaring Penerbangan Berdasarkan Harga                 | 7         |
| 7   | Melihat Informasi Detail Penerbangan                   | 9         |
| 8   | Memilih Kursi Pada Penerbangan                          | 8         |
| 9   | Memasukkan Data Penumpang                               | 9         |
| 10  | Memilih Layanan Tambahan (bagasi tambahan, makanan, dll.) | 7         |
| 11  | Mengisi Informasi Kontak                                | 6         |
| 12  | Memilih Metode Pembayaran                               | 9         |
| 13  | Melakukan Pembayaran dengan Kartu Kredit                | 9         |
| 14  | Melakukan Pembayaran dengan Transfer Bank               | 8         |
| 15  | Menerima Konfirmasi Pembayaran                          | 9         |
| 16  | Mengunduh Tiket Elektronik                              | 8         |
| 17  | Membatalkan Penerbangan                                 | 6         |
| 18  | Mengubah Jadwal Penerbangan                             | 7         |
| 19  | Mengajukan Pengembalian Dana                            | 7         |
| 20  | Mengelola Informasi Akun                               | 8         |


# 2.Mampu mendemonstrasikan web service (CRUD) dari produk digitalnya
**koneksi database**
```
<?php 
	$host = "localhost";
	$username = "root";
	$password = "";
	$dbname = "database_traveloka";
	$conn = mysqli_connect($host, $username, $password, $dbname) or die (mysqli_error());


// Memeriksa koneksi
if ($conn->connect_error) {
    die("Koneksi gagal: " . $conn->connect_error);
}


// Memeriksa permintaan pemesanan tiket
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nama = $_POST["nama"];
    $tanggal = $_POST["tanggal"];
    $jumlah_tiket = $_POST["jumlah_tiket"];

    // Menyimpan data pemesanan tiket ke database
    $sql = "INSERT INTO pemesanan_tiket (nama, tanggal, jumlah_tiket) VALUES ('$nama', '$tanggal', $jumlah_tiket)";
    if ($conn->query($sql) === TRUE) {
        echo "Pemesanan tiket berhasil";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
?>
```
**RESTful api**
1. **create**
```
<?php
// Mendefinisikan route untuk endpoint create
$app->post('/pemesanan', function () use ($app, $db) {
    // Mendapatkan data yang dikirim melalui body request
    $data = $app->request->getBody();

    // Mengubah data JSON menjadi array asosiatif
    $pemesanan = json_decode($data, true);

    // Melakukan validasi data

    // Memasukkan data pemesanan ke dalam database

    // Mengembalikan response

    // Contoh response jika berhasil
    $response = array(
        "status" => "success",
        "message" => "Pemesanan tiket berhasil dibuat"
    );

    // Mengubah response menjadi format JSON
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode($response);
});
?>
```
2. **read**

```
<?php
// Mendefinisikan route untuk endpoint read
$app->get('/histori', function () use ($app, $db) {
    // Mendapatkan data histori pemesanan tiket dari database

    // Mengubah data hasil query menjadi array asosiatif
    $histori = array();

    if ($resultHistori->num_rows > 0) {
        while ($rowHistori = $resultHistori->fetch_assoc()) {
            $histori[] = array(
                "nama" => $rowHistori["nama"],
                "tanggal" => $rowHistori["tanggal"],
                "jumlah_tiket" => $rowHistori["jumlah_tiket"]
            );
        }

        // Mengembalikan response dengan data histori
        $response = array(
            "status" => "success",
            "data" => $histori
        );
    } else {
        // Mengembalikan response jika tidak ada histori pemesanan tiket
        $response = array(
            "status" => "success",
            "message" => "Tidak ada histori pemesanan tiket"
        );
    }

    // Mengubah response menjadi format JSON
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode($response);
});
?>
```

3. **update**

```
<?php
// Mendefinisikan route untuk endpoint update
$app->put('/pemesanan/:id', function ($id) use ($app, $db) {
    // Mendapatkan data yang dikirim melalui body request
    $data = $app->request->getBody();

    // Mengubah data JSON menjadi array asosiatif
    $pemesanan = json_decode($data, true);

    // Melakukan validasi data

    // Memperbarui data pemesanan di dalam database berdasarkan ID

    // Mengembalikan response

    // Contoh response jika berhasil
    $response = array(
        "status" => "success",
        "message" => "Pemesanan tiket berhasil diperbarui"
    );

    // Mengubah response menjadi format JSON
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode($response);
});
?>
```
4. **delete**

```
<?php
// Mendefinisikan route untuk endpoint delete
$app->delete('/pemesanan/:id', function ($id) use ($app, $db) {
    // Menghapus data pemesanan dari database berdasarkan ID

    // Mengembalikan response

    // Contoh response jika berhasil
    $response = array(
        "status" => "success",
        "message" => "Pemesanan tiket berhasil dihapus"
    );

    // Mengubah response menjadi format JSON
    $app->response->headers->set('Content-Type', 'application/json');
    echo json_encode($response);
});
?>

```


**GraphQl**

```
mutation CreatePemesanan($data: PemesananInput!) {
  createPemesanan(data: $data) {
    id
    nama
    tanggal
    jumlahTiket
  }
}

# Contoh body request
{
  "data": {
    "nama": "John Doe",
    "tanggal": "2023-07-16",
    "jumlahTiket": 2
  }
}
```

****

# 3.Mampu mendemonstrasikan minimal 3 visualisasi data untuk business intelligence produk digitalnya

1. **grafik pemesanan harian**

```
<h2>Grafik Pemesanan Harian</h2>
<canvas id="dailyBookingsChart"></canvas>

<script>
    // Mendapatkan data pemesanan harian dari backend
    const dataPemesananHarian = [50, 80, 120, 90, 100, 70, 110];

    // Mendefinisikan konteks dan konfigurasi grafik bar
    const ctx = document.getElementById('dailyBookingsChart').getContext('2d');
    const barChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab', 'Min'],
            datasets: [{
                label: 'Pemesanan',
                data: dataPemesananHarian,
                backgroundColor: 'rgba(54, 162, 235, 0.5)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>
```
2. **Diagram Donat (Pie Chart) Kategori Pemesanan**

```
<h2>Persentase Pemesanan Berdasarkan Kategori</h2>
<canvas id="bookingCategoryChart"></canvas>

<script>
    // Mendapatkan data persentase pemesanan berdasarkan kategori dari backend
    const dataPersentaseKategori = {
        "Pesawat": 45,
        "Hotel": 30,
        "Paket Liburan": 15,
        "Aktivitas": 10
    };

    // Mendefinisikan konteks dan konfigurasi diagram donat
    const ctx = document.getElementById('bookingCategoryChart').getContext('2d');
    const pieChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: Object.keys(dataPersentaseKategori),
            datasets: [{
                data: Object.values(dataPersentaseKategori),
                backgroundColor: ['rgba(255, 99, 132, 0.5)', 'rgba(54, 162, 235, 0.5)', 'rgba(255, 205, 86, 0.5)', 'rgba(75, 192, 192, 0.5)'],
                borderColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)', 'rgba(255, 205, 86, 1)', 'rgba(75, 192, 192, 1)'],
                borderWidth: 1
            }]
        }
    });
</script>
```
3. **Diagram Garis (Line Chart) Pertumbuhan Pengguna**

```
 <h2>Tren Pertumbuhan Pengguna</h2>
<canvas id="userGrowthChart"></canvas>

<script>
    // Mendapatkan data pertumbuhan pengguna bulanan dari backend
    const dataPertumbuhanPengguna = [1000, 1500, 2000, 1800, 2500, 3000, 2800];

    // Mendefinisikan konteks dan konfigurasi diagram garis
    const ctx = document.getElementById('userGrowthChart').getContext('2d');
    const lineChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul'],
            datasets: [{
                label: 'Pengguna',
                data: dataPertumbuhanPengguna,
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 2,
                fill: false
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>
```


# 4.Mampu mendemonstrasikan penggunaan minimal 3 built-in function dengan ketentuan 

1. json_encode(): Fungsi ini digunakan untuk mengubah data menjadi format JSON. Dalam kode di atas, fungsi json_encode() digunakan untuk mengubah response menjadi format JSON sebelum mengirimkannya sebagai output ke klien

```
$response = array(
    "status" => "success",
    "message" => "Pemesanan tiket berhasil dibuat"
);

// Mengubah response menjadi format JSON
$jsonResponse = json_encode($response);
echo $jsonResponse;
```
2. fetch_assoc(): Fungsi ini digunakan untuk mengambil baris data hasil query sebagai array asosiatif dalam PHP. Dalam kode di atas, fungsi fetch_assoc() digunakan dalam loop while untuk mengambil setiap baris data histori pemesanan tiket dari hasil query. 


```
if ($resultHistori->num_rows > 0) {
    while ($rowHistori = $resultHistori->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $rowHistori["nama"] . "</td>";
        echo "<td>" . $rowHistori["tanggal"] . "</td>";
        echo "<td>" . $rowHistori["jumlah_tiket"] . "</td>";
        echo "</tr>";
    }
}
```
3. num_rows(): Fungsi ini digunakan untuk mengembalikan jumlah baris hasil query. Dalam kode di atas, fungsi num_rows() digunakan untuk memeriksa apakah ada histori pemesanan tiket yang ditemukan atau tidak.


```
if ($resultHistori->num_rows > 0) {
    // Ada histori pemesanan tiket
} else {
    // Tidak ada histori pemesanan tiket
}
```

# 5.Mampu mendemonstrasikan dan menjelaskan penggunaan Subquery pada produk digitalnya

1. **Pencarian Hotel dengan Harga Terendah dalam Suatu Lokasi**
```
SELECT *
FROM hotel
WHERE harga = (
    SELECT MIN(harga)
    FROM hotel
    WHERE lokasi = 'Jakarta'
);
```
2. **Statistik Penerbangan dalam Rangkaian Perjalanan**
```
SELECT t.id_perjalanan, t.nama_perjalanan, (
    SELECT COUNT(*)
    FROM penerbangan
    WHERE id_perjalanan = t.id_perjalanan
) AS jumlah_penerbangan
FROM perjalanan t;
```
3. **Pencarian Aktivitas Terpopuler dalam Suatu Destinasi**
```
SELECT *
FROM aktivitas
WHERE id_aktivitas = (
    SELECT id_aktivitas
    FROM pemesanan_aktivitas
    GROUP BY id_aktivitas
    ORDER BY COUNT(*) DESC
    LIMIT 1
);
```

# 6.Mampu mendemonstrasikan dan menjelaskan penggunaan Transaction pada produk digitalnya

Misalkan Anda memiliki aplikasi Traveloka yang melibatkan beberapa tabel seperti booking, passenger, dan payment. Saat pengguna memesan tiket, Anda ingin memastikan bahwa semua operasi yang terkait berhasil atau dibatalkan jika terjadi kegagalan. Berikut adalah contoh penggunaan

```
try {
    // Memulai transaksi
    $db->beginTransaction();

    // Langkah 1: Memasukkan data pemesanan ke dalam tabel booking
    $bookingQuery = "INSERT INTO booking (user_id, flight_id, date) VALUES (:userId, :flightId, :date)";
    $bookingStmt = $db->prepare($bookingQuery);
    $bookingStmt->bindParam(':userId', $userId);
    $bookingStmt->bindParam(':flightId', $flightId);
    $bookingStmt->bindParam(':date', $date);
    $bookingStmt->execute();

    // Mendapatkan ID booking yang baru saja dimasukkan
    $bookingId = $db->lastInsertId();

    // Langkah 2: Memasukkan data penumpang ke dalam tabel passenger
    foreach ($passengers as $passenger) {
        $passengerQuery = "INSERT INTO passenger (booking_id, name, age) VALUES (:bookingId, :name, :age)";
        $passengerStmt = $db->prepare($passengerQuery);
        $passengerStmt->bindParam(':bookingId', $bookingId);
        $passengerStmt->bindParam(':name', $passenger['name']);
        $passengerStmt->bindParam(':age', $passenger['age']);
        $passengerStmt->execute();
    }

    // Langkah 3: Memasukkan data pembayaran ke dalam tabel payment
    $paymentQuery = "INSERT INTO payment (booking_id, amount, status) VALUES (:bookingId, :amount, :status)";
    $paymentStmt = $db->prepare($paymentQuery);
    $paymentStmt->bindParam(':bookingId', $bookingId);
    $paymentStmt->bindParam(':amount', $amount);
    $paymentStmt->bindParam(':status', $status);
    $paymentStmt->execute();

    // Commit transaksi jika semua query berhasil
    $db->commit();
    echo "Pemesanan tiket berhasil!";
} catch (PDOException $e) {
    // Rollback transaksi jika terjadi kegagalan
    $db->rollBack();
    echo "Pemesanan tiket gagal: " . $e->getMessage();
}
```
Dengan menggunakan transaksi, Anda dapat memastikan bahwa semua operasi yang terkait berhasil atau dibatalkan secara atomik. Jika terjadi kegagalan pada salah satu langkah, semua perubahan yang dilakukan dalam transaksi akan dibatalkan, sehingga menjaga konsistensi data dalam sistem Traveloka.

# 7.Mampu mendemonstrasikan dan menjelaskan penggunaan Procedure / Function dan Trigger pada produk digitalnya

1.** Procedure**
Procedure adalah kumpulan pernyataan SQL yang diberi nama dan dapat dipanggil dari kode aplikasi atau perintah SQL lainnya. 
```
CREATE PROCEDURE GetFlightDetails(IN flightId INT)
BEGIN
    SELECT * FROM flights WHERE id = flightId;
END;
```
2. **Function**
Function adalah objek yang mengembalikan nilai berdasarkan logika yang didefinisikan di dalamnya. Function biasanya digunakan untuk melakukan perhitungan atau transformasi data dan dapat dipanggil dari query SQL atau kode aplikasi.
```
CREATE FUNCTION CalculateTotalPrice(in_flightId INT, in_quantity INT) RETURNS DECIMAL(10,2)
BEGIN
    DECLARE unitPrice DECIMAL(10,2);
    SELECT price INTO unitPrice FROM flights WHERE id = in_flightId;
    RETURN unitPrice * in_quantity;
END;
```
3. **Trigger**
Trigger adalah objek yang dieksekusi secara otomatis ketika suatu peristiwa tertentu terjadi pada tabel, seperti operasi INSERT, UPDATE, atau DELETE. Trigger memungkinkan Anda untuk menjalankan logika tambahan sebelum atau setelah operasi pada tabel.
```
CREATE TRIGGER UpdateAvailableSeats AFTER INSERT ON bookings
FOR EACH ROW
BEGIN
    UPDATE flights
    SET available_seats = available_seats - NEW.number_of_seats
    WHERE id = NEW.flight_id;
END;
```

# 8.Mampu mendemonstrasikan Data Control Language (DCL) pada produk digitalnya

1. Memberikan Izin pada Pengguna untuk Mengakses Tabel:
Misalkan Anda ingin memberikan izin kepada pengguna dengan nama 'didi' untuk membaca dan menulis pada tabel 'bookings'. Anda dapat menggunakan pernyataan GRANT untuk memberikan izin tersebut.

`GRANT SELECT, INSERT, UPDATE, DELETE ON bookings TO didi;`

2. Mencabut Izin dari Pengguna untuk Mengakses Tabel:
Misalkan Anda ingin mencabut izin pengguna dengan nama 'sari' untuk membaca dan menulis pada tabel 'flights'. Anda dapat menggunakan pernyataan REVOKE untuk mencabut izin tersebut.

`REVOKE SELECT, INSERT, UPDATE, DELETE ON flights FROM sari;`


# 9.Mampu mendemonstrasikan dan menjelaskan constraint yang digunakan pada produk digitalnya

1. **Primary Key Constraint**
Primary key constraint digunakan untuk memastikan bahwa nilai pada kolom tertentu dalam sebuah tabel adalah unik dan tidak boleh null.
```
CREATE TABLE flights (
    id INT PRIMARY KEY,
    airline VARCHAR(50),
    destination VARCHAR(50),
    departure_time DATETIME,
    -- Kolom lainnya
);
```
2. **Foreign Key Constraint**
Foreign key constraint digunakan untuk memastikan integritas referensial antara dua tabel.
```
CREATE TABLE bookings (
    id INT PRIMARY KEY,
    flight_id INT,
    -- Kolom lainnya
    FOREIGN KEY (flight_id) REFERENCES flights(id)
);
```
3. **Check Constraint**
Check constraint digunakan untuk memeriksa apakah nilai pada kolom memenuhi kriteria tertentu.
```
CREATE TABLE passengers (
    id INT PRIMARY KEY,
    booking_id INT,
    name VARCHAR(50),
    age INT,
    -- Kolom lainnya
    CHECK (age >= 18)
);
```
Dalam produk digital Traveloka, constraint digunakan untuk memastikan konsistensi data, menjaga hubungan antara tabel, dan membatasi nilai yang diperbolehkan. Dengan menggunakan constraint, Anda dapat memastikan bahwa data yang disimpan dalam sistem Traveloka memenuhi persyaratan bisnis dan integritas data yang diinginkan.

# 11. Bonus: Mendemonstrasikan UI untuk CRUDnya
CREATE
![dokumentasi](dokumentasi/create.png)
**read**
![dokumentasi](dokumentasi/read.png)

**DELETE**
![dokumentasi](dokumentasi/delete.png)
