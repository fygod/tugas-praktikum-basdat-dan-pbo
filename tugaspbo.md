# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat.
~~~~
import java.util.Scanner;

class Flight {
    private String origin;
    private String destination;

    public Flight(String origin, String destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    public void displayFlightInfo() {
        System.out.println("Anda mencari tiket pesawat dari " + origin + " ke " + destination + ".");
    }
}

class Hotel {
    private String location;

    public Hotel(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void displayHotelInfo() {
        System.out.println("Anda mencari hotel di " + location + ".");
    }
}

public class TravelokaApp {
    private static Scanner scanner = new Scanner(System.in);
    private static boolean isRunning = true;

    public static void main(String[] args) {
        displayWelcomeMessage();

        while (isRunning) {
            int option = getUserOption();

            switch (option) {
                case 1:
                    searchFlights();
                    break;
                case 2:
                    searchHotels();
                    break;
                case 3:
                    bookVacationPackage();
                    break;
                case 4:
                    flightInformation();
                    break;
                case 5:
                    hotelInformation();
                    break;
                case 6:
                    exitProgram();
                    break;
                default:
                    System.out.println("Opsi yang Anda pilih tidak valid.");
                    break;
            }
        }
    }

    private static void displayWelcomeMessage() {
        System.out.println("Selamat datang di Traveloka!");
        System.out.println("Silakan pilih opsi:");
        System.out.println("1. Pencarian tiket pesawat");
        System.out.println("2. Pencarian hotel");
        System.out.println("3. Pemesanan paket liburan");
        System.out.println("4. Informasi penerbangan");
        System.out.println("5. Informasi hotel");
        System.out.println("6. Keluar");
    }

    private static int getUserOption() {
        int option = 0;
        boolean validInput = false;

        while (!validInput) {
            try {
                System.out.print("Pilihan Anda: ");
                option = scanner.nextInt();
                scanner.nextLine();
                validInput = true;
            } catch (Exception e) {
                System.out.println("Input tidak valid. Silakan coba lagi.");
                scanner.nextLine();
            }
        }

        return option;
    }

    private static void searchFlights() {
        System.out.println("Anda sedang mencari tiket pesawat.");
        String origin = getCityInput("Masukkan kota asal: ");
        String destination = getCityInput("Masukkan kota tujuan: ");

        Flight flight = new Flight(origin, destination);
        flight.displayFlightInfo();

        // Tampilkan menu informasi pencarian tiket pesawat
        System.out.println("Menu Informasi Pencarian Tiket Pesawat:");
        System.out.println("1. Harga tiket");
        System.out.println("2. Waktu keberangkatan");
        System.out.println("3. Rute penerbangan");

        int flightInfoOption = getIntegerInput("Masukkan nomor menu informasi yang ingin Anda lihat: ");

        switch (flightInfoOption) {
            case 1:
                System.out.println("Informasi harga tiket pesawat.");
                // Implementasi logika untuk menampilkan informasi harga tiket pesawat
                // Misalnya:
                System.out.println("Harga tiket pesawat dari " + origin + " ke " + destination + " adalah Rp 1.000.000.");
                break;
            case 2:
                System.out.println("Informasi waktu keberangkatan pesawat.");
                // Implementasi logika untuk menampilkan informasi waktu keberangkatan pesawat
                // Misalnya:
                System.out.println("Pesawat ke " + destination + " berangkat pada pukul 09:00.");
                break;
            case 3:
                System.out.println("Informasi rute penerbangan pesawat.");
                // Implementasi logika untuk menampilkan informasi rute penerbangan pesawat
                // Misalnya:
                System.out.println("Rute penerbangan dari " + origin + " ke " + destination + " adalah langsung.");
                break;
            default:
                System.out.println("Opsi yang Anda pilih tidak valid.");
                break;
        }
    }

    private static void searchHotels() {
        System.out.println("Anda sedang mencari hotel.");
        String location = getStringInput("Masukkan lokasi: ");

        Hotel hotel = new Hotel(location);
        hotel.displayHotelInfo();

        // Tampilkan menu informasi pencarian hotel
        System.out.println("Menu Informasi Pencarian Hotel:");
        System.out.println("1. Harga hotel");
        System.out.println("2. Fasilitas hotel");
        System.out.println("3. Lokasi hotel");

        int hotelInfoOption = getIntegerInput("Masukkan nomor menu informasi yang ingin Anda lihat: ");

        switch (hotelInfoOption) {
            case 1:
                System.out.println("Informasi harga hotel.");
                // Implementasi logika untuk menampilkan informasi harga hotel
                // Misalnya:
                System.out.println("Harga hotel di " + location + " adalah Rp 500.000 per malam.");
                break;
            case 2:
                System.out.println("Informasi fasilitas hotel.");
                // Implementasi logika untuk menampilkan informasi fasilitas hotel
                // Misalnya:
                System.out.println("Hotel di " + location + " menyediakan fasilitas kolam renang, restoran, dan spa.");
                break;
            case 3:
                System.out.println("Informasi lokasi hotel.");
                // Implementasi logika untuk menampilkan informasi lokasi hotel
                // Misalnya:
                System.out.println("Hotel di " + location + " terletak di pusat kota dengan akses mudah ke tempat wisata.");
                break;
            default:
                System.out.println("Opsi yang Anda pilih tidak valid.");
                break;
        }
    }

    private static void bookVacationPackage() {
        System.out.println("Anda sedang memesan paket liburan.");
        String destination = getStringInput("Masukkan tujuan liburan: ");
        String departureDate = getDateInput("Masukkan tanggal keberangkatan (format: dd/mm/yyyy): ");
        int duration = getIntegerInput("Masukkan durasi liburan (dalam hari): ");

        // Implementasi logika pemesanan paket liburan berdasarkan tujuan, tanggal, dan durasi
        System.out.println("Anda memesan paket liburan ke " + destination + " pada tanggal " + departureDate + " dengan durasi " + duration + " hari.");
    }

    private static void flightInformation() {
        System.out.println("Anda ingin mendapatkan informasi penerbangan.");
        String origin = getCityInput("Masukkan kota asal: ");
        String destination = getCityInput("Masukkan kota tujuan: ");

        Flight flight = new Flight(origin, destination);
        flight.displayFlightInfo();

        // Implementasi logika untuk mendapatkan informasi penerbangan
        // Misalnya:
        System.out.println("Penerbangan dari " + origin + " ke " + destination + " tersedia pada tanggal 25 Mei 2023.");
    }

    private static void hotelInformation() {
        System.out.println("Anda ingin mendapatkan informasi hotel.");
        String location = getStringInput("Masukkan lokasi: ");

        Hotel hotel = new Hotel(location);
        hotel.displayHotelInfo();

        // Implementasi logika untuk mendapatkan informasi hotel
        // Misalnya:
        System.out.println("Hotel di " + location + " tersedia pada tanggal 25 Mei 2023.");
    }

    private static void exitProgram() {
        System.out.println("Terima kasih telah menggunakan Traveloka. Sampai jumpa!");
        isRunning = false;
    }

    private static String getCityInput(String message) {
        String city = "";

        while (city.isEmpty()) {
            System.out.print(message);
            city = scanner.nextLine().trim();
        }

        return city;
    }

    private static String getStringInput(String message) {
        String input = "";

        while (input.isEmpty()) {
            System.out.print(message);
            input = scanner.nextLine().trim();
        }

        return input;
    }

    private static int getIntegerInput(String message) {
        int input = 0;
        boolean validInput = false;

        while (!validInput) {
            try {
                System.out.print(message);
                input = scanner.nextInt();
                scanner.nextLine();
                validInput = true;
            } catch (Exception e) {
                System.out.println("Input tidak valid. Silakan coba lagi.");
                scanner.nextLine();
            }
        }

        return input;
    }

    private static String getDateInput(String message) {
        String date = "";

        while (date.isEmpty()) {
            System.out.print(message);
            date = scanner.nextLine().trim();
        }

        return date;
    }
}
~~~~
Program Traveloka yang diberikan adalah aplikasi sederhana untuk mencari tiket pesawat, mencari hotel, memesan paket liburan, serta mendapatkan informasi penerbangan dan hotel. Namun, program tersebut tidak melibatkan penggunaan pendekatan matematika secara langsung.

Program tersebut lebih berfokus pada pengolahan input dari pengguna, pemanggilan metode pada objek Flight dan Hotel, serta tampilan output berdasarkan pilihan yang dibuat oleh pengguna. Pendekatan matematika dan algoritma pemrograman yang lebih kompleks tidak digunakan dalam program tersebut.

Jika Anda memiliki contoh masalah spesifik yang ingin dipecahkan dengan pendekatan matematika atau algoritma pemrograman, silakan berikan informasi lebih lanjut, dan saya akan berusaha membantu Anda dengan penyelesaiannya.

# 2. Mampu menjelaskan algoritma dari solusi yang dibuat 

Algoritma yang digunakan dalam program Traveloka yang diberikan adalah sebagai berikut:
- Algoritma disini membuat solusi untuk memesan tiket

1. Metode main

Program dimulai dengan menampilkan pesan selamat datang dan opsi yang tersedia.
Dilakukan pengulangan selama isRunning bernilai true.
Pengguna diminta untuk memasukkan pilihan opsi.
Berdasarkan pilihan opsi yang dimasukkan, akan dipanggil metode yang sesuai.

~~public static void main(String[] args) {
        displayWelcomeMessage();

        while (isRunning) {
            int option = getUserOption();

            switch (option) {
                case 1:
                    searchFlights();
                    break;
                case 2:
                    searchHotels();
                    break;
                case 3:
                    bookVacationPackage();
                    break;
                case 4:
                    flightInformation();
                    break;
                case 5:
                    hotelInformation();
                    break;
                case 6:
                    exitProgram();
                    break;
                default:
                    System.out.println("Opsi yang Anda pilih tidak valid.");
                    break;
            }
        }
    }~~

~~~~

2. membuat displayWelcomeMessage

Menampilkan pesan selamat datang dan daftar opsi yang tersedia.

~~private static void displayWelcomeMessage() {
        System.out.println("Selamat datang di Traveloka!");
        System.out.println("Silakan pilih opsi:");
        System.out.println("1. Pencarian tiket pesawat");
        System.out.println("2. Pencarian hotel");
        System.out.println("3. Pemesanan paket liburan");
        System.out.println("4. Informasi penerbangan");
        System.out.println("5. Informasi hotel");
        System.out.println("6. Keluar");
    }
~~

3. membuat getUserOption

Meminta pengguna untuk memasukkan pilihan opsi.
Validasi input pengguna untuk memastikan input yang diterima adalah angka.
Mengembalikan pilihan opsi yang dimasukkan oleh pengguna.

~~private static int getUserOption() {
        int option = 0;
        boolean validInput = false;

        while (!validInput) {
            try {
                System.out.print("Pilihan Anda: ");
                option = scanner.nextInt();
                scanner.nextLine();
                validInput = true;
            } catch (Exception e) {
                System.out.println("Input tidak valid. Silakan coba lagi.");
                scanner.nextLine();
            }
        }

        return option;
    }~~

4. membuat searchFlights

Menampilkan pesan bahwa pengguna sedang mencari tiket pesawat.
Meminta pengguna untuk memasukkan kota asal dan kota tujuan.
Membuat objek Flight dengan kota asal dan tujuan yang dimasukkan.
Menampilkan informasi penerbangan berdasarkan opsi yang dipilih oleh pengguna.

~~private static void searchFlights() {
        System.out.println("Anda sedang mencari tiket pesawat.");
        String origin = getCityInput("Masukkan kota asal: ");
        String destination = getCityInput("Masukkan kota tujuan: ");

        Flight flight = new Flight(origin, destination);
        flight.displayFlightInfo();

        // Tampilkan menu informasi pencarian tiket pesawat
        System.out.println("Menu Informasi Pencarian Tiket Pesawat:");
        System.out.println("1. Harga tiket");
        System.out.println("2. Waktu keberangkatan");
        System.out.println("3. Rute penerbangan");

        int flightInfoOption = getIntegerInput("Masukkan nomor menu informasi yang ingin Anda lihat: ");

        switch (flightInfoOption) {
            case 1:
                System.out.println("Informasi harga tiket pesawat.");
                // Implementasi logika untuk menampilkan informasi harga tiket pesawat
                // Misalnya:
                System.out.println("Harga tiket pesawat dari " + origin + " ke " + destination + " adalah Rp 1.000.000.");
                break;
            case 2:
                System.out.println("Informasi waktu keberangkatan pesawat.");
                // Implementasi logika untuk menampilkan informasi waktu keberangkatan pesawat
                // Misalnya:
                System.out.println("Pesawat ke " + destination + " berangkat pada pukul 09:00.");
                break;
            case 3:
                System.out.println("Informasi rute penerbangan pesawat.");
                // Implementasi logika untuk menampilkan informasi rute penerbangan pesawat
                // Misalnya:
                System.out.println("Rute penerbangan dari " + origin + " ke " + destination + " adalah langsung.");
                break;
            default:
                System.out.println("Opsi yang Anda pilih tidak valid.");
                break;
        }
    }~~

5. Membuat searchHotels

Menampilkan pesan bahwa pengguna sedang mencari hotel.
Meminta pengguna untuk memasukkan lokasi.
Membuat objek Hotel dengan lokasi yang dimasukkan.
Menampilkan informasi hotel berdasarkan opsi yang dipilih oleh pengguna.

~~private static void searchHotels() {
        System.out.println("Anda sedang mencari hotel.");
        String location = getStringInput("Masukkan lokasi: ");

        Hotel hotel = new Hotel(location);
        hotel.displayHotelInfo();

        // Tampilkan menu informasi pencarian hotel
        System.out.println("Menu Informasi Pencarian Hotel:");
        System.out.println("1. Harga hotel");
        System.out.println("2. Fasilitas hotel");
        System.out.println("3. Lokasi hotel");

        int hotelInfoOption = getIntegerInput("Masukkan nomor menu informasi yang ingin Anda lihat: ");

        switch (hotelInfoOption) {
            case 1:
                System.out.println("Informasi harga hotel.");
                // Implementasi logika untuk menampilkan informasi harga hotel
                // Misalnya:
                System.out.println("Harga hotel di " + location + " adalah Rp 500.000 per malam.");
                break;
            case 2:
                System.out.println("Informasi fasilitas hotel.");
                // Implementasi logika untuk menampilkan informasi fasilitas hotel
                // Misalnya:
                System.out.println("Hotel di " + location + " menyediakan fasilitas kolam renang, restoran, dan spa.");
                break;
            case 3:
                System.out.println("Informasi lokasi hotel.");
                // Implementasi logika untuk menampilkan informasi lokasi hotel
                // Misalnya:
                System.out.println("Hotel di " + location + " terletak di pusat kota dengan akses mudah ke tempat wisata.");
                break;
            default:
                System.out.println("Opsi yang Anda pilih tidak valid.");
                break;
        }
    }~~

6. Membuat bookVacationPackage
- solusi dibawah ini digunakan untuk membooking pesanan
Menampilkan pesan bahwa pengguna sedang memesan paket liburan.
Meminta pengguna untuk memasukkan tujuan liburan, tanggal keberangkatan, dan durasi liburan.
Melakukan pemrosesan pemesanan paket liburan berdasarkan input yang diberikan oleh pengguna.

~~private static void bookVacationPackage() {
        System.out.println("Anda sedang memesan paket liburan.");
        String destination = getStringInput("Masukkan tujuan liburan: ");
        String departureDate = getDateInput("Masukkan tanggal keberangkatan (format: dd/mm/yyyy): ");
        int duration = getIntegerInput("Masukkan durasi liburan (dalam hari): ");

        // Implementasi logika pemesanan paket liburan berdasarkan tujuan, tanggal, dan durasi
        System.out.println("Anda memesan paket liburan ke " + destination + " pada tanggal " + departureDate + " dengan durasi " + duration + " hari.");
    }~~

7. membuat flightInformation:

Menampilkan pesan bahwa pengguna ingin mendapatkan informasi penerbangan.
Meminta pengguna untuk memasukkan kota asal dan kota tujuan.
Membuat objek Flight dengan kota asal dan tujuan yang dimasukkan.
Menampilkan informasi penerbangan berdasarkan input yang diberikan oleh pengguna.

~~private static void flightInformation() {
        System.out.println("Anda ingin mendapatkan informasi penerbangan.");
        String origin = getCityInput("Masukkan kota asal: ");
        String destination = getCityInput("Masukkan kota tujuan: ");

        Flight flight = new Flight(origin, destination);
        flight.displayFlightInfo();

        // Implementasi logika untuk mendapatkan informasi penerbangan
        // Misalnya:
        System.out.println("Penerbangan dari " + origin + " ke " + destination + " tersedia pada tanggal 25 Mei 2023.");
    }
~~

8. Membuat hotelInformation

Menampilkan pesan bahwa pengguna ingin mendapatkan informasi hotel.
Meminta pengguna untuk memasukkan lokasi.
Membuat objek Hotel dengan lokasi yang dimasukkan.
Menampilkan informasi hotel berdasarkan input yang diberikan oleh pengguna.

~~private static void hotelInformation() {
        System.out.println("Anda ingin mendapatkan informasi hotel.");
        String location = getStringInput("Masukkan lokasi: ");

        Hotel hotel = new Hotel(location);
        hotel.displayHotelInfo();

        // Implementasi logika untuk mendapatkan informasi hotel
        // Misalnya:
        System.out.println("Hotel di " + location + " tersedia pada tanggal 25 Mei 2023.");
    }~~

9. Membuat exitProgram

Menampilkan pesan terima kasih dan mengubah nilai isRunning menjadi false agar program keluar dari pengulangan utama.

~~private static void exitProgram() {
        System.out.println("Terima kasih telah menggunakan Traveloka. Sampai jumpa!");
        isRunning = false;
    }~~

10. Membuat getCityInput

Meminta pengguna untuk memasukkan nama kota.
Melakukan validasi untuk memastikan input tidak kosong.
Mengembalikan nama kota yang dimasukkan.

~~private static String getCityInput(String message) {
        String city = "";

        while (city.isEmpty()) {
            System.out.print(message);
            city = scanner.nextLine().trim();
        }

        return city;
    }~~

11. Membuat getStringInput:

Meminta pengguna untuk memasukkan input berupa string.
Melakukan validasi untuk memastikan input tidak kosong.
Mengembalikan input yang dimasukkan.

~~ private static String getStringInput(String message) {
        String input = "";

        while (input.isEmpty()) {
            System.out.print(message);
            input = scanner.nextLine().trim();
        }

        return input;
    }~~

12. Membuat getIntegerInput:

Meminta pengguna untuk memasukkan input berupa angka.
Melakukan validasi untuk memastikan input adalah angka.
Mengembalikan angka yang dimasukkan.

~~ private static int getIntegerInput(String message) {
        int input = 0;
        boolean validInput = false;

        while (!validInput) {
            try {
                System.out.print(message);
                input = scanner.nextInt();
                scanner.nextLine();
                validInput = true;
            } catch (Exception e) {
                System.out.println("Input tidak valid. Silakan coba lagi.");
                scanner.nextLine();
            }
        }

        return input;
    }
~~

13. Membuat getDateInput:

Meminta pengguna untuk memasukkan tanggal dalam format tertentu.
Melakukan validasi untuk memastikan input tidak kosong.
Mengembalikan tanggal yang dimasukkan.

~~ private static String getDateInput(String message) {
        String date = "";

        while (date.isEmpty()) {
            System.out.print(message);
            date = scanner.nextLine().trim();
        }

        return date;
    }
}
~~~~
# 3.Mampu menjelaskan konsep dasar OOP

OOP sangat penting di perusahaan karena menurut saya dari cara kerjanya yang mudah dipahami dan terstruktur dalam membuat program. OOP membantu kita mengatur kode program dengan rapi, mirip seperti mengelompokkan benda-benda dalam kotak.

Misalnya ituu di perusahaan aplikasi perbankan, OOP memungkinkan kita membuat objek-objek seperti "Akun" yang berisi informasi tentang nama, saldo, dan transaksi. Kemudian kita juga bisa membuat objek "Pelanggan" yang berisi data pribadi pelanggan dan daftar akun yang dimilikinya.

~~

# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat
penggunaan encapsulation dalam kelas Flight dan Hotel. Atribut origin dan destination pada kelas Flight serta atribut location pada kelas Hotel dideklarasikan sebagai private, yang berarti hanya dapat diakses secara langsung oleh kelas itu sendiri.

```
class Flight {
    private String origin;
    private String destination;

    public Flight(String origin, String destination) {
        this.origin = origin;
        this.destination = destination;
    }
```


Kemudian, terdapat metode getter getOrigin() dan getDestination() pada kelas Flight, serta metode getter getLocation() pada kelas Hotel. Metode-metode ini digunakan untuk mengakses nilai dari atribut yang bersifat private dari luar kelas. Dengan menggunakan metode getter, kita dapat membaca nilai atribut tersebut tanpa perlu mengakses langsung atributnya.

```
    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }
```
Selain itu, terdapat pula metode displayFlightInfo() pada kelas Flight dan metode displayHotelInfo() pada kelas Hotel. Metode-metode ini digunakan untuk menampilkan informasi terkait penerbangan dan hotel ke layar.

```
    public void displayFlightInfo() {
        System.out.println("Anda mencari tiket pesawat dari " + origin + " ke " + destination + ".");
    }
}

```


# 5.Mampu mendemonstrasikan penggunaan Abstraction secara tepat

Abstraksi mengacu pada kemampuan untuk menentukan entitas abstrak yang menggambarkan karakteristik dan perilaku umum dari objek-objek yang terkait. Dalam konteks kode di atas, pilar abstraksi terlihat dalam penggunaan kelas abstrak TravelService dan implementasi metode-metode abstrak di kelas turunannya.


```
abstract class TravelService {
    // ...
    public abstract void displayServiceInfo();
    public abstract void search();
    public abstract void displayInformation();
    // ...
}
```

Di sini, kelas TravelService dideklarasikan sebagai kelas abstrak menggunakan kata kunci abstract. Ini berarti kelas tersebut tidak dapat diinisialisasi menjadi objek secara langsung, tetapi digunakan sebagai kerangka kerja untuk kelas turunannya.

Kelas TravelService memiliki tiga metode abstrak yaitu displayServiceInfo(), search(), dan displayInformation(). Metode-metode ini tidak memiliki implementasi di kelas abstrak, tetapi diharapkan untuk diimplementasikan oleh kelas-kelas turunannya.

```
class Flight extends TravelService {
    // ...
    public void displayServiceInfo() {
        // Implementasi khusus untuk menampilkan informasi layanan penerbangan
    }

    public void search() {
        // Implementasi khusus untuk melakukan pencarian penerbangan
    }

    public void displayInformation() {
        // Implementasi khusus untuk menampilkan informasi penerbangan
    }
    // ...
}

class Hotel extends TravelService {
    // ...
    public void displayServiceInfo() {
        // Implementasi khusus untuk menampilkan informasi layanan hotel
    }

    public void search() {
        // Implementasi khusus untuk melakukan pencarian hotel
    }

    public void displayInformation() {
        // Implementasi khusus untuk menampilkan informasi hotel
    }
    // ...
}
```
kelas Flight dan Hotel merupakan kelas turunan dari TravelService yang mengimplementasikan metode-metode abstrak yang ditentukan di kelas abstrak tersebut. Setiap kelas turunan memiliki implementasi khusus sesuai dengan jenis layanan yang mereka wakili. Misalnya, metode displayServiceInfo() pada kelas Flight akan menampilkan informasi tentang layanan penerbangan, sedangkan metode displayServiceInfo() pada kelas Hotel akan menampilkan informasi tentang layanan hotel.


# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat

Inheritance

`abstract class TravelService {
    // ...
}

class Flight extends TravelService {
    // ...
}

class Hotel extends TravelService {
    // ...
}

class VacationPackage extends TravelService {
    // ...
}
`
Dalam kode di atas, kita memiliki kelas abstrak TravelService sebagai superclass atau kelas induk. Kemudian, kita mendefinisikan tiga kelas turunan yaitu Flight, Hotel, dan VacationPackage yang mewarisi atau meng-extend kelas TravelService.

polimorfisme

`TravelService flightService = new Flight("Jakarta", "Bali");
TravelService hotelService = new Hotel("Bali");
TravelService vacationPackageService = new VacationPackage();

flightService.displayServiceInfo();
flightService.search();
flightService.displayInformation();

hotelService.displayServiceInfo();
hotelService.search();
hotelService.displayInformation();

vacationPackageService.displayServiceInfo();
vacationPackageService.search();
vacationPackageService.displayInformation();
`
Dalam contoh di atas, kita membuat objek flightService dengan tipe TravelService, tetapi sebenarnya objek tersebut adalah instance dari kelas Flight. Hal yang sama juga berlaku untuk objek hotelService yang merupakan instance dari kelas Hotel, dan objek vacationPackageService yang merupakan instance dari kelas VacationPackage.

# 7.Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Proses bisnis dapat diterjemahkan ke dalam skema OOP dengan langkah-langkah berikut:
1. user dapat melakukan pencarian tiket
ketika user ingin menggunakan OneDrive maka langkah utama adalah melakukan registrasi karena jika tidak melakukan registrasi tidak akan dapat mengaakses Traveloka.
Disini saya membuaat class Register yang mempunyai fungsi untuk membuat sekaligus menmpung akun yang telah di buat.


class Flight extends TravelService {
    private String origin;
    private String destination;

    public Flight(String origin, String destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public String getOrigin() {
        return origin;
    }

    public String getDestination() {
        return destination;
    }

    @Override
    public void displayServiceInfo() {
        System.out.println("Anda mencari tiket pesawat dari " + origin + " ke " + destination + ".");
    }

    @Override
    public void search() {
        // Implementasi logika pencarian tiket pesawat
        System.out.println("Mencari tiket pesawat dari " + origin + " ke " + destination + "...");
    }

    @Override
    public void displayInformation() {
        // Implementasi logika untuk menampilkan informasi tiket pesawat
        System.out.println("Informasi tiket pesawat dari " + origin + " ke " + destination + "...");
    }
}

2. user dapat mencari hotel menggunakan username dan password yang telah di registrasi.
Kemudian saya membuat class login untuk memverifikasi akun akun yang telah di buat.
class Hotel extends TravelService {
    private String location;

    public Hotel(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public void displayServiceInfo() {
        System.out.println("Anda mencari hotel di " + location + ".");
    }

    @Override
    public void search() {
        // Implementasi logika pencarian hotel
        System.out.println("Mencari hotel di " + location + "...");
    }

    @Override
    public void displayInformation() {
        // Implementasi logika untuk menampilkan informasi hotel
        System.out.println("Informasi hotel di " + location + "...");
    }
}

3. user dapat memesan paket liburan
 private static void bookVacationPackage() {
        System.out.println("Anda sedang memesan paket liburan.");
        String destination = getStringInput("Masukkan tujuan liburan: ");
        String departureDate = getDateInput("Masukkan tanggal keberangkatan (format: dd/mm/yyyy): ");
        int duration = getIntegerInput("Masukkan durasi liburan (dalam hari): ");

        // Implementasi logika pemesanan paket liburan berdasarkan tujuan, tanggal, dan durasi
        System.out.println("Anda memesan paket liburan ke " + destination + " pada tanggal " + departureDate + " dengan durasi " + duration + " hari.");
    }

    private static void displayInformation(TravelService service) {
        service.displayServiceInfo();
        service.displayInformation();
    }

4. user dapat mendapatkan informasi tentang tiket pesawat
 @Override
    public void displayInformation() {
        // Implementasi logika untuk menampilkan informasi tiket pesawat
        System.out.println("Informasi tiket pesawat dari " + origin + " ke " + destination + "...");
    }
5. user mendapatkan informasi tentang hotel

    @Override
    public void displayInformation() {
        // Implementasi logika untuk menampilkan informasi hotel
        System.out.println("Informasi hotel di " + location + "...");
    }



# 8.Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table 


- Class Diagram

```mermaid
classDiagram
    class Penerbangan {
        -kodePenerbangan: String
        -maskapai: String
        -jadwal: DateTime
        -harga: float
        -rute: String
        +getKodePenerbangan(): String
        +getMaskapai(): String
        +getJadwal(): DateTime
        +getHarga(): float
        +getRute(): String
    }

    class PencarianPenerbangan {
        +cariPenerbangan(rute: String, jadwal: DateTime): List<Penerbangan>
    }

    class MembandingkanHarga {
        +bandingkanHarga(penerbangan: List<Penerbangan>): Penerbangan
    }

    class InformasiPenerbangan {
        +getInformasiPenerbangan(kodePenerbangan: String): Penerbangan
    }

    class Pembayaran {
        +pilihMetodePembayaran(): void
        +lakukanPembayaran(metodePembayaran: String, totalPembayaran: float): boolean
        +terimaKonfirmasiPembayaran(): void
    }

    class TiketElektronik {
        -kodeTiket: String
        -penumpang: String
        -penerbangan: Penerbangan
        +getKodeTiket(): String
        +getPenumpang(): String
        +getPenerbangan(): Penerbangan
        +unduhTiketElektronik(): void
    }

    class PembatalanPenerbangan {
        +batalkanPenerbangan(kodeTiket: String): void
        +ubahJadwalPenerbangan(kodeTiket: String, jadwalBaru: DateTime): void
    }

    class PengelolaAkun {
        +kelolaInformasiAkun(): void
        +mengelolaPengaturanLainnya(): void
    }

    class PemesananTiket {
        +pesanTiket(penerbangan: Penerbangan, penumpang: String): TiketElektronik
    }

    class KursiPenerbangan {
        -nomorKursi: String
        +getNomorKursi(): String
    }

    class Penumpang {
        -nama: String
        -umur: int
        -jenisKelamin: String
        +getNama(): String
        +getUmur(): int
        +getJenisKelamin(): String
    }

    PencarianPenerbangan --> Penerbangan
    MembandingkanHarga --> Penerbangan
    InformasiPenerbangan --> Penerbangan
    PemesananTiket --> Penerbangan
    PemesananTiket --> Penumpang
    PemesananTiket --> TiketElektronik
    Pembayaran --> TiketElektronik
    TiketElektronik --> Penerbangan
    PembatalanPenerbangan --> TiketElektronik
    PengelolaAkun --> Penumpang
    PemesananTiket --> KursiPenerbangan

```
- Table Usecase

| No. | Use Case                                                | Prioritas |
|-----|--------------------------------------------------------|-----------|
| 1   | Pencarian Penerbangan                                  | 9         |
| 2   | Membandingkan Harga Penerbangan                        | 8         |
| 3   | Menyimpan Penerbangan Favorit                          | 6         |
| 4   | Menyaring Penerbangan Berdasarkan Maskapai              | 6         |
| 5   | Menyaring Penerbangan Berdasarkan Jadwal                | 7         |
| 6   | Menyaring Penerbangan Berdasarkan Harga                 | 7         |
| 7   | Melihat Informasi Detail Penerbangan                   | 9         |
| 8   | Memilih Kursi Pada Penerbangan                          | 8         |
| 9   | Memasukkan Data Penumpang                               | 9         |
| 10  | Memilih Layanan Tambahan (bagasi tambahan, makanan, dll.) | 7         |
| 11  | Mengisi Informasi Kontak                                | 6         |
| 12  | Memilih Metode Pembayaran                               | 9         |
| 13  | Melakukan Pembayaran dengan Kartu Kredit                | 9         |
| 14  | Melakukan Pembayaran dengan Transfer Bank               | 8         |
| 15  | Menerima Konfirmasi Pembayaran                          | 9         |
| 16  | Mengunduh Tiket Elektronik                              | 8         |
| 17  | Membatalkan Penerbangan                                 | 6         |
| 18  | Mengubah Jadwal Penerbangan                             | 7         |
| 19  | Mengajukan Pengembalian Dana                            | 7         |
| 20  | Mengelola Informasi Akun                               | 8         |


# 9.Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)


# 10.Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)
